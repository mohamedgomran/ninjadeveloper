// require all controllers
const contactRouter = require('./controllers/contactsController')

// use all controllers as routers
const addRoutes = (server)=>{
	server.use('/contacts', contactRouter)
}

module.exports = addRoutes