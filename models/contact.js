const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')
const mongoosePaginate = require('mongoose-paginate')
const validator = require('validator')
const Schema = mongoose.Schema


const contact = new Schema({
	email:{
		type: String,
		required: [true, 'Email is required!'],
		validate: [(email)=>validator.isEmail(email), "Not valid email!"]
	},
	mobile:{
		type: String,
		required: [true, 'Mobile is required!'],
		validate: [(mob)=>validator.isMobilePhone(mob, 'ar-EG'), "Not valid mobile!"]
	},
	firstName:{
		type: String,
		required: [true, 'First Name is required!'],
		validate: [(fname)=>validator.isAlpha(fname), 
		"First name must only contains letters a-zA-Z!"]
	},
	lastName:{
		type: String,
		required: [true, 'Last Name is required!'],
		validate: [(lname)=>validator.isAlpha(lname), 
		"Last name must only contains letters a-zA-Z!"]
	},
	userId:{
		type: Number,
	},
})

// add contact and check the uniqueness of the email
// validate the contact against validation rules in the model's schema
contact.statics.addContact = async (contact)=>{
	oldContact = await contactModel.findOne({email:contact.email})
	if (oldContact) {
		return Promise.reject({email:"Email already exist"})
	}

	newContact = new contactModel(contact)
	contactValidation = newContact.validateSync()
	if (!contactValidation){
		return newContact.save()
	}
	else {
		return Promise.reject(contactValidation.errors)
	}
}

// get recent 5 contacts, sorted decending by contact _id
contact.statics.getRecentList = (body)=>{
	const {userId} = body
	return contactModel.find(
		{
			userId:userId
		},
		{},
		{
			limit:5,
			sort: {
				_id:-1
			}
		}
		)
}

// get list of a users' contacts
// response is paginated - 5 contact/page - and can be filterer using contact firstName
contact.statics.getList = (body)=>{
	let {char, pageNum, userId} = body
	regex = char ? new RegExp(`^${char}`, 'i') : new RegExp()
	return contactModel.paginate(
			{
				userId: userId, 
				firstName: regex
			},
			{
				page: pageNum, 
				limit: 5
			})
}


// initialize autoincrement plugin
contact.plugin(mongoosePaginate)
autoIncrement.initialize(mongoose.connection)
contact.plugin(autoIncrement.plugin, {model:'contact', startAt:1, field:'_id', incrementBy: 1})

// register the model
const contactModel = mongoose.model("contact", contact);

module.exports = contactModel
