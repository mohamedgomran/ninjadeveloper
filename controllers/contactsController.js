const express = require('express')
const router = express.Router()
const contactModel = require('../models/contact')

router.post('/addContact', (req, res)=>{
	contactModel.addContact(req.body)
	.then(data => {
		res.send({
			statusCode: 200,
			message: "Contact added",
			data: data
		})
	})
	.catch(err => {
		res.send({
			statusCode: 400,
			message: "Error adding contact",
			errors: err
		})
	})
})

router.post('/getList', (req, res)=>{
  	contactModel.getList(req.body)
	.then(data => {
		res.send({
			statusCode: 200,
			message: "Contacts list",
			data: data
		})
	})
	.catch(err => {
		res.send({
			statusCode: 400,
			message: "Error getting contacts list",
			errors: err
		})
	})
})

router.post('/getRecentList', (req, res)=>{
  contactModel.getRecentList(req.body)
  .then(data => {
		res.send({
			statusCode: 200,
			message: "Recent contacts list",
			data: data
		})
 	})
  .catch(err => {
		res.send({
			statusCode: 400,
			message: "Error getting recent contacts list",
			errors: err
		})
	})
})

module.exports = router