const express = require('express')
const mongoose = require('mongoose')
const server = express()
const bodyParser = require('body-parser')
const JSONParsermid = bodyParser.json();
const addRoutes = require('./routes')
const authMid = require('./middlewares/authMid')

// connect to mongoose
mongoose.connect("mongodb://localhost/Ninja")

// parse json data
server.use(JSONParsermid)
// auth middlware
server.use(authMid)
// add routes
addRoutes(server)

module.exports = server