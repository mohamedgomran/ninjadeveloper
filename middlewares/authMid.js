const users = [
	{
		authorization:'1',
		deviceToken:'12',
		fingerPrint: '123',
		name: 'user 1',
		id:1,
	},
	{
		authorization:'12',
		deviceToken:'123',
		fingerPrint: '1234',
		name: 'user 2',
		id: 2,
	}
]

// auth function, check user credentials
module.exports = (req, res, next)=>{
	const {authorization, deviceToken, fingerPrint} = req.body

    if (authorization && deviceToken && fingerPrint) {
    	users.forEach((user)=>{
    		if (authorization === user.authorization 
    			&& deviceToken === user.deviceToken 
    			&& fingerPrint === user.fingerPrint) {
    			req.body.userId = user.id
    		}
    	})
    }
    
    if(req.body.userId){
    	next()
    } else {
		res.send({
			statusCode: 400,
			message: "Not authorized",
		})
    }
}