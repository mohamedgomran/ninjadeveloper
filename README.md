# Ninja Developer

Vapulus back-end developer task

## Prerequisites (Used Technologies)

* Node.js v8.10.0 or above
* mongoDB database engine v3.6.3 or above


## Installing

1- Clone the repository

2- In the NodeJs folder run:
```
	npm install
	npm start
```
3- You can hit the available 3 APIs using POSTMAN

## Examples
localhost:9000/contacts/addContact
example of sent data:
```
	{
		"email":"aa@aavv.com",
		"mobile":"01009401788",
		"firstName":"ahmed",
		"lastName":"mohamed",
		"authorization":"1",
		"deviceToken":"12",
		"fingerPrint": "123"
	}
```

localhost:9000/contacts/getList
example of sent data:
```
	{
		"pageNum":1,
		"authorization":"1",
		"deviceToken":"12",
		"fingerPrint": "123"
	}
```
localhost:9000/contacts/getRecentList
example of sent data:
```
	{
		"authorization":"1",
		"deviceToken":"12",
		"fingerPrint": "123"
	}
```


## Authors

* Mohamed Omran [mohamedgomran](https://bitbucket.org/mohamedgomran)
